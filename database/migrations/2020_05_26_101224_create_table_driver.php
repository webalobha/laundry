<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDriver extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->bigInteger('phone_number')->nullable()->index();
            $table->string('country_id')->default(0)->index();
            $table->string('state_id')->default(0)->index();
            $table->string('city')->nullable()->index();
            $table->string('email')->index();
            $table->string('avtar')->index()->nullable();
            $table->string('avtar_path')->index()->nullable();
            $table->string('company_name')->index()->nullable();
            $table->string('gst_number')->index()->nullable();
            $table->string('longitude')->index()->nullable();
            $table->string('latitude')->index()->nullable();
            $table->text('address')->nullable();
            $table->string('password')->nullable()->index();
            $table->string('remender_token')->nullable()->index();
            $table->string('api_token')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver');
    }
}
