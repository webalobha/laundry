<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('service_id')->index();
            $table->unsignedInteger('service_type_id')->index()->default(0);
            $table->string('name')->index();
            $table->string('item_image')->nullable()->index();
            $table->boolean('status')->index()->default(false);
            $table->boolean('is_variant_avail')->index()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_items');
    }
}
