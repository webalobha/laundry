<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->index()->nullable()->after('id');
            $table->string('middle_name')->index()->nullable()->after('first_name');
            $table->string('last_name')->index()->nullable()->after('middle_name');
            $table->string('country_code')->index()->nullable()->after('last_name');
            $table->string('mobile_number')->index()->nullable()->after('country_code');
            $table->string('email')->index()->nullable()->after('mobile_number');
            $table->boolean('status')->default(true)->index()->after('email');
            $table->string('age')->index()->nullable()->after('status');
            $table->unsignedInteger('gender')->index()->nullable()->after('age');
            $table->string('latitude')->index()->nullable()->after('gender');
            $table->string('longitude')->index()->nullable()->after('latitude');
            $table->string('android_key')->index()->nullable()->after('longitude');
            $table->string('ios_key')->index()->nullable()->after('android_key');
            $table->string('password')->after('ios_key')->nullable();
            $table->string('api_token', 80)->after('password')->unique()->nullable()->default(null);
            $table->softDeletes()->after('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['first_name', 'middle_name', 'last_name', 'country_code', 'mobile_number', 'email', 'status', 'age', 'gender', 'latitude', 'longitude', 'android_key', 'ios_key', 'password', 'api_token']);
        });
    }
}
