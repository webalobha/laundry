<?php

use App\Model\Service;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serviceNames = ['Wash and Fold', 'Dry Clean'];

        # Store Data to model
        foreach ($serviceNames as $key => $serviceName) {
        	Service::updateOrCreate(['name' => $serviceName,'service_type_available' => '1','status' => '1']);
        }
    }
}
