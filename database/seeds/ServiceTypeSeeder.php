<?php

use App\Model\ServiceType;
use Illuminate\Database\Seeder;

class ServiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serviceType = ['Hang Dry', 'Beddings'];
       
        # Store Data to model
        foreach ($serviceType as $key => $serviceType) {
        	ServiceType::updateOrCreate(['service_name' => $serviceType,'service_id' => '1']);
        }
    }
}
