<?php

use App\Model\SupportTicketStatus;
use Illuminate\Database\Seeder;

class SupportStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # initialize Support Statuses
        $supportStatuses = ['Open', 'Resolved'];

        # Store Data to model
        foreach ($supportStatuses as $key => $supportStatus) {
        	SupportTicketStatus::updateOrCreate(['name' => $supportStatus]);
        }
    }
}
