//   var validator = $( "#myform" ).validate();

// Destroy the Role data.
$(document).on('click', 'button#user-destroy', function(e) {
  e.preventDefault();

    swal({
        title         : "Are You Sure",
        text          : "Do You Want To Remove User",
        icon          : "warning",
        buttons       : true,
        dangerMode    : true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url         : $(this).attr("href"),
              method      : 'delete',
              dataType    : "json",
              success     : function(response) {
                  if (response.success == 200) {

                    swal("Good job!", ""+response.message+"", "success");

                    setInterval(function(){
                      location.reload(true);
                    }, 2000);

                  } // end success if
              } // end success function.
          }); // end ajax .
        } else {
          // Write something here.
        }
    }); // End then.
}); // end Document.

// validator.resetForm();
$(function() {
  $("form[name='add-user']").validate({
    rules : {
      first_name    : "required",
      last_name     : "required",
      email         : {
                        email     : true
                      },
      password          : "required",
      confirm_password  : {
                            required      : true,
                            equalTo       : '#password'
                          },
      mobile_number     : {
                            digits        : true,
                            minlength     : 10
                          },
      user_name         : {
                            required        : true,
                            minlength       : 5,
                            remote          : {
                                                  url         : $("#meta-url").attr('name')+"/admin/users/username-check",
                                                  type        : "post"
                                               }
                            // data            : {
                            //                     username: function() {
                            //                     return $( "#username" ).val();
                            //                   }
                          }
    },

    messages: {
      first_name        :  "First name is required",
      last_name         :  "Last name is required",
      email             :  "Enter Valid Email-Address",
      password          :  "Password is required",
      confirm_password  : {
                              required        : "Confirm-Password is required",
                              equalTo         : "Confirm-Password is not matching from password",
                          },
      mobile_number     : {
                              digits          : "Enter Only Digits(123)",
                              minlength       : "Your mobile should be at least 10 digits long"
                          },
      user_name         : {
                              required        : "User name is required",
                              minlength       : "Your User name should be at atleast 5 character",
                              remote          : "User-Name already in record"
                          }
    }

  });
});

// Edit User
$(function() {
  $("form[name='edit-user']").validate({
    rules : {
      first_name    : "required",
      last_name     : "required",
      email         : {
                        email     : true
                      },
      confirm_password  : {
                            equalTo       : '#password'
                          },
      mobile_number     : {
                            digits        : true,
                            minlength     : 10
                          }
    },

    messages: {
      first_name        :  "First name is required",
      last_name         :  "Last name is required",
      email             :  "Enter Valid Email-Address",
      confirm_password  : {
                              equalTo         : "Confirm-Password is not matching from password",
                          },
      mobile_number     : {
                              digits          : "Enter Only Digits(123)",
                              minlength       : "Your mobile should be at least 10 digits long"
                          }
    }
    
  });
});

// validator.resetForm();
$(function() {
  $("form[name='add-route']").validate({
    rules : {
      name          : "required",
      longitude     : "required",
      latitude      : "required",
    },

    messages: {
      name           :  "Route Name is required",
      longitude      :  "Longitude is required",
      latitude       :  "Latitude is required",
    }

  });
});

// Edit User
$(function() {
  $("form[name='edit-route']").validate({
    rules : {
      name          : "required",
      longitude     : "required",
      latitude      : "required",
    },

    messages: {
      name           :  "Route Name is required",
      longitude      :  "Longitude is required",
      latitude       :  "Latitude is required",
    }

  });
});

// Destroy the Role data.
$(document).on('click', 'button#route-destroy', function(e) {
  e.preventDefault();

    swal({
        title         : "Are You Sure",
        text          : "Do You Want To Remove Route",
        icon          : "warning",
        buttons       : true,
        dangerMode    : true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url         : $(this).attr("href"),
              method      : 'delete',
              dataType    : "json",
              success     : function(response) {
                  if (response.success == 200) {

                    swal("Good job!", ""+response.message+"", "success");

                    setInterval(function(){
                      location.reload(true);
                    }, 2000);

                  } // end success if
              } // end success function.
          }); // end ajax .
        } else {
          // Write something here.
        }
    }); // End then.
}); // end Document.

// validator.resetForm();
$(function() {
  $("form[name='add-store']").validate({
    rules : {
      name                : "required",
      route_id            : "required",
      channel_type_id     : "required",
      trade_profile_id    : "required",
      longitude           : "required",
      latitude            : "required",
    },

    messages: {
      name                :  "Store Name is required",
      route_id            :  "Route Name is required",
      channel_type_id     :  "Channel Type is required",
      trade_profile_id    :  "Trade Profile is required",
      longitude           :  "Longitude is required",
      latitude            :  "Latitude is required",
    }

  });
});

// Edit User
$(function() {
  $("form[name='edit-store']").validate({
    rules : {
      name                : "required",
      route_id            : "required",
      channel_type_id     : "required",
      trade_profile_id    : "required",
      longitude           : "required",
      latitude            : "required",
    },

    messages: {
      name                :  "Store Name is required",
      route_id            :  "Route Name is required",
      channel_type_id     :  "Channel Type is required",
      trade_profile_id    :  "Trade Profile is required",
      longitude           :  "Longitude is required",
      latitude            :  "Latitude is required",
    }

  });
});

// Destroy the Role data.
$(document).on('click', 'button#store-destroy', function(e) {
  e.preventDefault();

    swal({
        title         : "Are You Sure",
        text          : "Do You Want To Remove Store",
        icon          : "warning",
        buttons       : true,
        dangerMode    : true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url         : $(this).attr("href"),
              method      : 'delete',
              dataType    : "json",
              success     : function(response) {
                  if (response.success == 200) {

                    swal("Good job!", ""+response.message+"", "success");

                    setInterval(function(){
                      location.reload(true);
                    }, 2000);

                  } // end success if
              } // end success function.
          }); // end ajax .
        } else {
          // Write something here.
        }
    }); // End then.
}); // end Document.

// Destroy the Role data.
$(document).on('click', 'button#role-destroy', function(e) {
  e.preventDefault();

    swal({
        title         : "Are You Sure",
        text          : "Do You Want To Remove Role",
        icon          : "warning",
        buttons       : true,
        dangerMode    : true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url         : $(this).attr("href"),
              method      : 'delete',
              dataType    : "json",
              success     : function(response) {
                  if (response.success == 200) {

                    swal("Good job!", ""+response.message+"", "success");

                    setInterval(function(){
                      location.reload(true);
                    }, 2000);

                  } else if(response.error == 404) {

                    swal("Warning!", ""+response.message+"", "warning");
                  }
              } // end success.
          }); // end ajax .
        } else {
          // Write something here.
        }
    }); // End then.
}); // end Document.

// Country create.
$(document).on('submit', 'form#country-create', function(e) {
  e.preventDefault();

  $.ajax({
    method       :  $(this).attr('method'),
    url          :  $(this).attr('action'),
    data         :  $(this).serialize(),
    dateType     :  'json',
    success      :  function(response) {
      if (response.success  == 200) {
        $('div.open_model').modal('hide');
        
        swal("Good job!", ""+response.message+"", "success");

        setInterval(function(){
          location.reload(true);
        }, 2000);

      } 
    } // End Success.
  }); // End Ajax.
}); // End Document.

// country Edit.
$(document).on('submit', 'form#country-edit', function(e) {
  e.preventDefault();

  $.ajax({
    method       :  $(this).attr('method'),
    url          :  $(this).attr('action'),
    data         :  $(this).serialize(),
    dateType     :  'json',
    success      :  function(response) {
      if (response.success  == 200) {
        $('div.open_model').modal('hide');
        
        swal("Good job!", ""+response.message+"", "success");

        setInterval(function(){
          location.reload(true);
        }, 2000);

      } 
    } // End Success.
  }); // End Ajax.
}); // End Document.

// Destroy the country data.
$(document).on('click', 'button#country-destroy', function(e) {
  e.preventDefault();

    swal({
        title         : "Are You Sure",
        text          : "Do You Want To Remove State",
        icon          : "warning",
        buttons       : true,
        dangerMode    : true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url         : $(this).attr("href"),
              method      : 'delete',
              dataType    : "json",
              success     : function(response) {
                  if (response.success == 200) {

                    swal("Good job!", ""+response.message+"", "success");

                    setInterval(function(){
                      location.reload(true);
                    }, 2000);

                  } else if (response.warning == 300) {

                    swal("Warning!", ""+response.message+"", "warning");

                  }// end if.
              } // end success.
          }); // end ajax .
        } else {
          // Write something here.
        }
    }); // End then.
}); // end Document.

// State create.
$(document).on('submit', 'form#state-create', function(e) {
  e.preventDefault();

  $.ajax({
    method       :  $(this).attr('method'),
    url          :  $(this).attr('action'),
    data         :  $(this).serialize(),
    dateType     :  'json',
    success      :  function(response) {
      if (response.success  == 200) {
        $('div.open_model').modal('hide');
        
        swal("Good job!", ""+response.message+"", "success");

        setInterval(function(){
          location.reload(true);
        }, 2000);

      } 
    } // End Success.
  }); // End Ajax.
}); // End Document.

// State Edit.
$(document).on('submit', 'form#state-edit', function(e) {
  e.preventDefault();

  $.ajax({
    method       :  $(this).attr('method'),
    url          :  $(this).attr('action'),
    data         :  $(this).serialize(),
    dateType     :  'json',
    success      :  function(response) {
      if (response.success  == 200) {
        $('div.open_model').modal('hide');
        
        swal("Good job!", ""+response.message+"", "success");

        setInterval(function(){
          location.reload(true);
        }, 2000);

      } 
    } // End Success.
  }); // End Ajax.
}); // End Document.

// Destroy the state data.
$(document).on('click', 'button#state-destroy', function(e) {
  e.preventDefault();

    swal({
        title         : "Are You Sure",
        text          : "Do You Want To Remove State",
        icon          : "warning",
        buttons       : true,
        dangerMode    : true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url         : $(this).attr("href"),
              method      : 'delete',
              dataType    : "json",
              success     : function(response) {
                  if (response.success == 200) {

                    swal("Good job!", ""+response.message+"", "success");

                    setInterval(function(){
                      location.reload(true);
                    }, 2000);

                  } // end if.
              } // end success.
          }); // end ajax .
        } else {
          // Write something here.
        }
    }); // End then.
}); // end Document.

// Tax create.
$(document).on('submit', 'form#tax-create', function(e) {
	e.preventDefault();

	$.ajax({
		method       :  $(this).attr('method'),
		url          :  $(this).attr('action'),
		data         :  $(this).serialize(),
		dateType     :  'json',
		success      :  function(response) {
			if (response.success  == 200) {
        $('div.open_model').modal('hide');
                
	      swal("Good job!", ""+response.message+"", "success");

        setInterval(function(){
          location.reload(true);
        }, 2000);

			} 
		} // End Success.
	}); // End Ajax.
}); // End Document.

// Tax Edit.
$(document).on('submit', 'form#tax-edit', function(e) {
	e.preventDefault();

	$.ajax({
		method       :  $(this).attr('method'),
		url          :  $(this).attr('action'),
		data         :  $(this).serialize(),
		dateType     :  'json',
		success      :  function(response) {
			if (response.success  == 200) {
        $('div.open_model').modal('hide');
                
	      swal("Good job!", ""+response.message+"", "success");

        setInterval(function(){
          location.reload(true);
        }, 2000);

			} 
		} // End Success.
	}); // End Ajax.
}); // End Document.

// Destroy the tax data.
$(document).on('click', 'button#tax-destroy', function(e) {
  e.preventDefault();

  	swal({
        title			    : "Are You Sure",
        text 			    : "Do You Want To Remove Tax",
        icon 			    : "warning",
        buttons 		  : true,
        dangerMode 		: true,
    })
  	.then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url 			   : $(this).attr("href"),
              method 		   : 'delete',
              dataType 		 : "json",
              success 		 : function(response) {
                  if (response.success == 200) {

  		        	     swal("Good job!", ""+response.message+"", "success");

  	                setInterval(function(){
  	                  location.reload(true);
  	                }, 2000);

                  } // end if.
              } // end success.
          }); // end ajax .
        } else {
          // Write something here.
        }
  	}); // End then.
}); // end Document.

// State Search.
$(document).on('change', 'select#country_id', function(event) {
    event.preventDefault();

    $.ajax({
        method    : 'get',
        url       : $("#meta-url").attr('name')+'/get-states',
        data      : { country_id : $(this).val() },
        dataType  : "json",
        success: function(response) {
          $('select#state_id').empty();
          if (response.success == 200) {
            $('select#state_id').append("<option value=''>Choose State</option>");
            $.each(response.states, function(key, value) {
                $('select#state_id').append("<option value="+value.id+">"+value.name+"</option>");
            });

          }
        }
    });
}); // End Document.

// Destroy the Product data.
$(document).on('click', 'button#product-destroy', function(e) {
  e.preventDefault();

    swal({
        title         : "Are You Sure",
        text          : "Do You Want To Remove Product",
        icon          : "warning",
        buttons       : true,
        dangerMode    : true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url         : $(this).attr("href"),
              method      : 'delete',
              dataType    : "json",
              success     : function(response) {
                  if (response.success == 200) {

                    swal("Good job!", ""+response.message+"", "success");

                    setInterval(function(){
                      location.reload(true);
                    }, 2000);

                  } // end success if
              } // end success function.
          }); // end ajax .
        } else {
          // Write something here.
        }
    }); // End then.
}); // end Document.
