// search product.
$(document).on('keyup', 'input#search-product', function(e) {
    e.preventDefault();
	var term  =  $(this).val();

    if (term.length <= 2) {
        $('#show_patient_name').empty();
        return false;
    }

    $.ajax({
  		    method      :  'GET',
        	url 		:  $("#meta-url").attr('name') +'/admin/search-product',
        	data        :  { term : term },
        	dataType 	:  "json",
        	success 	: function(response) {
        		if (response.success == 200) {
                    $('#show_patient_name').empty();
                    $('#show_patient_name').append(response.output.html_content);
        		} else if(response.warning == 300) {
                    swal(response.message)
                    $('input#search-product').val('');
                    $('#show_patient_name').empty();
                }
        	}
    });
});

// get ad product row.
function setProductId(product_id) {
    event.preventDefault();

    // count product table row.
    var tableRow  = $('table#add-product-row >tbody >tr ').length + parseInt(100);

    $.ajax({
            method      :  'GET',
            url         :  $("#meta-url").attr('name') +'/admin/add-product-row',
            data        :  { tableRow : tableRow, product_id : product_id },
            dataType    :  "json",
            success     : function(response) {
                if (response.success == 200) {
                    $('#add-product-row').append(response.output.html_content);
                    $('#show_patient_name').empty();
                    $('input#search-product').val('');
                    updateTableFooterTotal();
                }
            }
    });
}


$(document).on('keyup', 'input#quantity', function(e){
    e.preventDefault();

    var rowTable  =  $(this).closest('tbody');
    updateProductRowData(rowTable);
    updateTableFooterTotal();

});

function updateProductRowData(rowTable) {

    // get input quantity and .
    var quantity        =  rowTable.find('input#quantity').val();

    if (quantity <= 0) {
        rowTable.find('input#quantity').val(1);
        return false;
    }

    var sellingPrice    =  rowTable.find('input#amount').val();

    var finalAmount     =  sellingPrice  *  quantity;
    rowTable.find('input#final_total').val(finalAmount.toFixed(2));

}

function updateTableFooterTotal() {

    var total_quantity      =   0;
    var total_amount        =   0;
    
    $('table#add-product-row').find('tbody').each( function() {
       
        total_quantity  += parseInt($(this).find('#quantity').val());
        total_amount    += parseFloat($(this).find('#final_total').val());
    });

    $('span.total_amount').text(total_amount.toFixed(2));
    $('input#total_amount').val(total_amount.toFixed(2));

    $('span.total_item').text(total_quantity);
}

$("table#add-product-row").on('click', '#add-product-row', function() {
    swal({
        title: 'Are You Sure',
        text: 'Do you want to remove',
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            // remove product row.
            $(this).parent().parent().parent().remove(); 
            updateTableFooterTotal();
        } else {
            // swal("Your imaginary file is safe!");   // You can give any message after not delete.
        }
    });
});