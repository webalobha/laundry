
// Product Category create.
$(document).on('submit', 'form#product-category-create', function(e) {
  e.preventDefault();

  $.ajax({
    method       :  $(this).attr('method'),
    url          :  $(this).attr('action'),
    data         :  $(this).serialize(),
    dateType     :  'json',
    success      :  function(response) {
      if (response.success  == 200) {
        $('div.open_model').modal('hide');
        
        swal("Good job!", ""+response.message+"", "success");

        setInterval(function(){
          location.reload(true);
        }, 2000);
      } 
    } // End Success.
  }); // End Ajax.
}); // End document.

// Product Category Edit.
$(document).on('submit', 'form#product-category-edit', function(e) {
  e.preventDefault();

  $.ajax({
    method       :  $(this).attr('method'),
    url          :  $(this).attr('action'),
    data         :  $(this).serialize(),
    dateType     :  'json',
    success      :  function(response) {
      if (response.success  == 200) {
        $('div.open_model').modal('hide');
              
        swal("Good job!", ""+response.message+"", "success");

        setInterval(function(){
          location.reload(true);
        }, 2000);

      } 
    } // End Success.
  }); // End Ajax.
}); // End document.

// Destroy the Product Category data.
$(document).on('click', 'button#product-category-destroy', function(e) {
  e.preventDefault();

    swal({
        title         : "Are You Sure",
        text          : "Do You Want To Remove Product Category",
        icon          : "warning",
        buttons       : true,
        dangerMode    : true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url         : $(this).attr("href"),
              method      : 'delete',
              dataType    : "json",
              success     : function(response) {
                  if (response.success == 200) {

                    swal("Good job!", ""+response.message+"", "success");

                    setInterval(function(){
                      location.reload(true);
                    }, 2000);

                  } else if (response.error == 404) {
                    swal("Warning!", ""+response.message+"", "warning");
                  }
              } // end success function.
          }); // end ajax .
        } else {
          // Write something here.
        }
    }); // End then.
}); // end document.

// Show hide parent category.
$(document).on('click', 'input#sub_category', function() {

  if ($(this).prop('checked')) {
    $('div#enabledParentId').attr('class', 'show col-md-12');
  } else {
    $('div#enabledParentId').attr('class', 'hidden');
  }

});

// Show hide inline and text area content.
$(document).on('change', 'select#productInformationContentType_id', function(e) {
  e.preventDefault();

  // Get the inline and text id.
  var productInformationContentType_id  = $(this).val();

  // Get the description id.
  var productInformationContent_id    = $(this).attr('data-product-information-content');

  // One for inline text. check and show hide the text div with description id.
  if (productInformationContentType_id  == 1) {

    $('div#enabledTextArea'+productInformationContent_id).attr('class', 'col-md-8 hidden');
    $('div#enabledInlineText'+productInformationContent_id).attr('class', 'col-md-8 show');

  } else if (productInformationContentType_id  == 2) {

    $('div#enabledTextArea'+productInformationContent_id).attr('class', 'col-md-8 show');
    $('div#enabledInlineText'+productInformationContent_id).attr('class', 'col-md-8 hidden');
  }

});

// Add Button More text.
$(document).on('click', 'button#add-more', function(e) {
  e.preventDefault();

  // Get the description id and name to set the input placeholder and appended input name for save into db.
  var productInformationContent_id    =  $(this).attr('data-button-information-content');
  var productInformationContent_name  =  $(this).attr('data-button-information-content-name');

  // Get appended length of div with his description id and increment one.
  var addMorelength  = $('.inline-text'+productInformationContent_id+' ').length + 100;

  // Append the more row inside the inline text.
  $('#add-more-text'+productInformationContent_id).append('<div class="form-group d-flex text-div inline-text'+productInformationContent_id+'"><input type="text" name="product_content['+productInformationContent_id+'][variation]['+addMorelength+'][content]" class="form-control form-control-sm" placeholder="'+productInformationContent_name+'"><button type="button" data-button-information-content="productInformationContent_id" class="btn btn-sm btn-danger ml-1" id="remove-row">-</button></div>');
  
});

// Remove the inline text.
$(document).on('click', '#remove-row', function(e) {
  e.preventDefault();
      $(this).closest('div.text-div').remove(); 
});

// validator.resetForm();
jQuery(document).ready(function () {
  jQuery("form[name='add-product']").validate({
    rules : {
      name                    : "required",
      product_category_id     : "required",
      description             : "required",
      amount                  : {
                                   number   :  true,
                                   required : true
                                },
    },

    messages: {
      name                    :  "Name is required",
      product_category_id     :  "Category is required",
      description             :  "Description is required",
      amount                  :  {
                                  number          : "Enter Only Number(123)",
                                  required        : "Amount is required"
                                 }
    }

  });

  // jQuery("input[type=file]").each(function() {
  //   jQuery(this).rules("add", {
  //       accept      : "png|jpe?g",
  //       messages    : {
  //           accept  : "Only jpeg, jpg or png images"
  //       }
  //   });
  // });
});

// Destroy the Product data.
$(document).on('click', 'button#product-destroy', function(e) {
  e.preventDefault();

    swal({
        title         : "Are You Sure",
        text          : "Do You Want To Remove Product",
        icon          : "warning",
        buttons       : true,
        dangerMode    : true,
    })
    .then((willDelete) => {
        if (willDelete) {
          $.ajax({
              url         : $(this).attr("href"),
              method      : 'delete',
              dataType    : "json",
              success     : function(response) {
                  if (response.success == 200) {

                    swal("Good job!", ""+response.message+"", "success");

                    setInterval(function(){
                      location.reload(true);
                    }, 2000);

                  } else if (response.error == 404) {
                    swal("Warning!", ""+response.message+"", "warning");
                  }
              } // end success function.
          }); // end ajax .
        } else {
          // Write something here.
        }
    }); // End then.
}); // end document.

// Multiple Image remove add.
$(document).ready(function() {
  if (window.File && window.FileList && window.FileReader) {

    $("#product-file").on("change", function(e) {
      var files     = e.target.files,
      filesLength   = files.length;

      for (var i = 0; i < filesLength; i++) {

        var f           = files[i]
        var fileReader  = new FileReader();

        fileReader.onload = (function(e) {

          var file = e.target;
          if (!/\.(jpe?g|png|gif|svg)$/i.test(f.name)) {

              alert(f.name +" is not an image");

          } else {

            $("<span class=\"file-reader-append-image-show pip\" >" +
                "<img class=\"image-show imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
                "<span class=\"remove-cursor remove\">Remove image</span>" +
                "</span>").insertAfter("#product-file");

            $(".remove").click(function() {
                $(this).parent(".pip").remove();
            });

          } // else end.
        });

        fileReader.readAsDataURL(f);
      } // for loop end.
    }); // end fetch product file.

  } else {
    alert("Your browser doesn't support to File API");
  } // else api end.
});

// show hide product wrap_available available.
$(document).on('click', 'input#wrap_available', function() {

  if ($(this).prop('checked')) {
    $('div#enabledWrap').attr('class', 'show col-md-4');
  } else {
    $('div#enabledWrap').attr('class', 'hidden');
  }

});