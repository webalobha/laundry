  // Number Only.
  function numbersonly(e) {
      var unicode=e.charCode? e.charCode : e.keyCode
      if (unicode!=8) { //if the key isn't the backspace key (which we should allow)
          if (unicode<48||unicode>57) //if not a number
            return false //disable key press
      }
  }
  
  // Alphabate Only.
  function ValidateAlpha(evt) {
    var keyCode = (evt.which) ? evt.which : evt.keyCode
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
    return false;  
  }
  
  // CSRF Token.
  $(document).ready(function(){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
  });

  // btn-modal.
  $(document).on( 'click', 'button.btn-modal', function(e){
      e.preventDefault();
      var container = $(this).data("container");

      $.ajax({
          url: $(this).data("href"),
          dataType: "html",
          success: function(result){
              $(container).html(result).modal('show');
          }
      });
  });

  // Link Modal.
  $(document).on('click', 'a.link-modal', function(e) {
    e.preventDefault();
      var container = $(this).data("container");

      $.ajax({
          url: $(this).attr("href"),
          dataType: "html",
          success: function(result){
              $('div.'+container).html(result).modal('show');
          }
      });
  });
  
  //Play notification sound on success, error and warning
  // toastr.options.onShown = function() {
  //     if($(this).hasClass('toast-success')){
  //         var audio = $("#success-audio")[0];
  //         if(audio !== undefined){
  //             audio.play();
  //         }
  //     } else if($(this).hasClass('toast-error')){
  //         var audio = $("#error-audio")[0]; 
  //         if(audio !== undefined){
  //             audio.play();
  //         }
  //     } else if($(this).hasClass('toast-warning')){
  //         var audio = $("#warning-audio")[0]; 
  //         if(audio !== undefined){
  //             audio.play();
  //         }
  //     }
  // }

  
// Date Range Picker
// $(function() {
//     var start = moment().subtract(29, 'days');
//     var end   = moment();

//     function cb(start, end) {
//         $('#daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
//     }

//     $('#daterange').daterangepicker({
//         startDate   : start,
//         endDate     : end,
//         ranges: {
//            'Today'          : [moment(), moment()],
//            'Yesterday'      : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
//            'Last 7 Days'    : [moment().subtract(6, 'days'), moment()],
//            'Last 30 Days'   : [moment().subtract(29, 'days'), moment()],
//            'This Month'     : [moment().startOf('month'), moment().endOf('month')],
//            'Last Month'     : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
//         }
//     }, cb);
//     cb(start, end);
// });


  // single with validation
  function previewImages() {

    var $preview = $('#previewsinglevalid').empty();
    if (this.files) $.each(this.files, readAndPreview);

    function readAndPreview(i, file) {
      
      if (!/\.(jpe?g|png|gif|svg)$/i.test(file.name)){
          alert(file.name +" is not an image");
          $('input#file-input-single').val('');
          return false;
      } 
      
      var reader = new FileReader();

      $(reader).on("load", function() {
        $preview.append($("<img/>", {src:this.result, height:100}));
      });

      reader.readAsDataURL(file);
      
    }
  }

  $('#file-input-single').on("change", previewImages);
  
  // Initialize Select2 Elements.
  $('.select2').select2();

  $(document).ready(function () {
      bsCustomFileInput.init();
  });