<!DOCTYPE html>
<html lang="en">
  <!-- Head start  -->
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    {{-- CSRF Token --}} 
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    {{-- directory path define for validate js --}}
    <meta id="meta-url" charset="utf-8" name="{{ url('') }}">

    {{-- Yield Define for title --}} 
    <title>
        @yield('title')
    </title>

    {{-- Yield Define for css --}}
    @yield('css')

    {{-- CSS Include --}}
    @include('layout.partials.css')

   </head>
  <!-- End head -->

  <!-- Body start -->
 <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

    {{-- Header Include --}}
    @include('layout.partials.header')

    {{-- Sidebar Include --}}
    @include('layout.partials.sidebar')

    @if(session()->has('success') || session()->has('error')) 
       @include('layout.partials.alertify_message') 
    @endif

    {{-- Yield Define for content --}}
    @yield('content')

   
    {{-- Sidebar Include --}}
    @include('layout.partials.footer')

    {{-- javascript Include --}}
    @include('layout.partials.js')

    {{-- Yield Define for javascript --}}
    @yield('javascript')

  </body>
  <!-- End body -->

</html>

