<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="{{ action('AdminController@index') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>

        <li><a href="{{route('viewServices')}}"><i class="fa fa-circle-o text-aqua"></i> <span>Services</span></a>
        </li>
        <li><a href="customer.php"><i class="fa fa-circle-o text-aqua"></i> <span>Customers</span></a></li>
        <li class="treeview"><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Providers</span></a>
             <ul class="treeview-menu">
            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Providers</span></a></li>
            <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Verify Providers</span></a></li>
        </ul>
        </li>
        <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Driver</span></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Orders</span></a></li>

        <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Rating & Review</span></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Subscription</span></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Provider Request</span></a></li>
        <li><a href="javascript:void(0);"><i class="fa fa-circle-o text-aqua"></i> <span>Promotions</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>


