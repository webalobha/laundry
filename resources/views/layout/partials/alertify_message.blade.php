    <script src="{{ asset('assets/admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="{{ URL::to('css/toastr.min.css')}}">
		<script src="{{ URL::to('js/toastr.min.js')}}"></script>

    @if(session()->has('success'))
      <script>
          toastr.success('<strong>{{session()->get('success')}}</strong>');
      </script>
    @elseif(session()->has('error'))
      <script>
        toastr.error('<strong>{{session()->get('error')}}</strong>');
      </script>
    @endif