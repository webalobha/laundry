<header class="main-header">
  <!-- Logo -->
  <a href="{{ action('AdminController@index') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels --> 
    <span class="logo-mini">
      <img src="{{ URL::to('assets/admin/dist/img/favicon.png')}}" style="width: 90%;">
    </span>
    <!-- logo for regular state and mobile devices --> 
    <span class="logo-lg">
      <img src="{{ URL::to('assets/admin/dist/img/logo.png')}}" style="height: 50px;">
    </span>
  </a>

  {{--- Get Auth User ---}}
   @php 
     $user = Auth('admin')->User() ?? ''; 
   @endphp
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"> <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset($user->avtar_path ?? '') }}" class="user-image" alt="User Image">
              <span class="hidden-xs">
               {{ $user->name ?? '' }}
              </span>
            </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="{{ asset($user->avtar_path ?? '') }}" class="img-circle" alt="User Image">
              <p>{{ $user->name ?? '' }} 
                <small> Member since {{ $user->created_at ?? '' }}</small>
              </p>
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left"> <a href="#" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right"> <a href="{{ action('AdminController@logout') }}" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
        <li> <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li>
      </ul>
    </div>
  </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->