<!-- jQuery 3 -->
<script src="{{ asset('assets/admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- datatables -->
<script src="{{ asset('assets/admin/bower_components/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/admin/bower_components/datatables-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/dist/js/adminlte.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('assets/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap  -->
<script src="{{ asset('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('assets/admin/bower_components/chart.js/Chart.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset('assets/admin/dist/js/pages/dashboard2.js') }}"></script>
<!-- CK Editor -->
<script src="{{ asset('assets/admin/bower_components/ckeditor/ckeditor.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('assets/admin/dist/js/demo.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/bower_components/datatables-bs/js/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/bower_components/datatables-bs/js/buttons.flash.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/bower_components/datatables-bs/js/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/bower_components/datatables-bs/js/pdfmake.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('assets/admin/bower_components/datatables-bs/js/vfs_fonts.js') }}"></script> -->
<script type="text/javascript" src="{{ asset('assets/admin/bower_components/datatables-bs/js/buttons.html5.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/bower_components/datatables-bs/js/buttons.print.min.js') }}"></script>
<script>
	$(function () {
	    CKEDITOR.replace('editor1')
	  })
</script>
<script>
	$(function (){
	    $('#example1').DataTable({
	      'paging'      : true,
	      'lengthChange': true,
	      'searching'   : true,
	      'ordering'    : true,
	      'info'        : true,
	      'autoWidth'   : true
	    })
	  })
</script>
<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            'copy', 'csv', 'excel', 'pdf', 'print'
	        ]
	    } );
	} );
</script>
<script type="text/javascript">
	//Date picker
	    $(function () {
	    $('#datepicker1').datepicker({
	      autoclose: true
	    })
	  });
	
	   //Date picker
	    $(function () {
	    $('#datepicker2').datepicker({
	      autoclose: true
	    })
	  });
</script>
<script type="text/javascript">
	//     $(function(){
	//     var current = location.pathname;
	//     $('.treeview a').each(function(){
	//         var $this = $(this);
	        
	//         if($this.attr('href').indexOf(current) !== -1){
	//             $this.addClass('active');
	//         }
	//     })
	// })
	//     $(function() {
	//      var url = window.location;
	//     const allLinks = document.querySelectorAll('.treeview a');
	//     const currentLink = [...allLinks].filter(e => {
	//       return e.href == url;
	//     });
	    
	//     currentLink[0].classList.add("active")
	//     currentLink[0].closest(".treeview").style.display="block";
	//     currentLink[0].closest(".treeview").classList.add("active");
	// });
	
	
	/** add active class and stay opened when selected */
	var url = window.location;
	
	// for sidebar menu entirely but not cover treeview
	$('ul.sidebar-menu a').filter(function() {
	   return this.href == url;
	}).parent().addClass('active');
	
	// for treeview
	$('ul.treeview-menu a').filter(function() {
	   return this.href == url;
	}).closest('.treeview').addClass('active');
</script>
<script>
	/************Number and dash allowed************/
	function dashnumbersonly(e) {
	        var unicode = e.charCode? e.charCode : e.keyCode
	        if (unicode!=8 && unicode!=45) { //if the key isn't the backspace key (which we should allow)
	            if (unicode<48||unicode>57) //if not a number
	            return false //disable key press
	        }
	    }
	/************Numbers only *************/
	        function numbersonly(e) {
	 var k = event ? event.which : window.event.keyCode;
	            if (k == 32) return false;
	                var unicode=e.charCode? e.charCode : e.keyCode;
	
	                if (unicode!=8) { //if the key isn't the backspace key (which we should allow)
	                    if (unicode<48||unicode>57) //if not a number
	                    return false //disable key press
	                }
	            }
	      
	/********************Number with decimal***********/
	
	$('.allownumer_decimal').keypress(function(event) {
	    if (event.which != 46 && (event.which < 47 || event.which > 59))
	    {
	        event.preventDefault();
	        if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
	            event.preventDefault();
	        }
	    }
	});
	    /************Alphabet only *************/
	        function alphaonly(evt) {
	          var keyCode = (evt.which) ? evt.which : evt.keyCode
	          if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
	          return false;  
	        }  
	
	    /************Alpha numeric only *************/   
	           function alphanumnotspace(e) {               
	            var specialKeys = new Array();
	            specialKeys.push(8); //Backspace
	            specialKeys.push(9); //Tab
	            specialKeys.push(46); //Delete
	            specialKeys.push(36); //Home
	            specialKeys.push(35); //End
	            specialKeys.push(37); //Left
	            specialKeys.push(39); //Right
	             var k = event ? event.which : window.event.keyCode;
	            if (k == 32) return false;
	            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
	            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
	            // document.getElementById("error").style.display = ret ? "none" : "inline";
	            return ret;
	        } 
	
	        function alphanumeric(e) {               
	            var specialKeys = new Array();
	            specialKeys.push(8); //Backspace
	            specialKeys.push(9); //Tab
	            specialKeys.push(46); //Delete
	            specialKeys.push(36); //Home
	            specialKeys.push(35); //End
	            specialKeys.push(37); //Left
	            specialKeys.push(39); //Right
	
	            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
	            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));
	            // document.getElementById("error").style.display = ret ? "none" : "inline";
	            return ret;
	        } 
	function ValidateEmail(inputText)
	{
	var mailformat = /^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,3})+$/;
	if(inputText.value.match(mailformat))
	{
	alert("You have entered a valid email address!");    //The pop up alert for a valid email address
	document.form1.text1.focus();
	return true;
	}
	else
	{
	alert("You have entered an invalid email address!");    //The pop up alert for an invalid email address
	document.form1.text1.focus();
	return false;
	}
	}
</script>
<script language="JavaScript">
	var ev = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
	            var x = document.getElementById("check");
	            function validate(email){
	            if(!ev.test(email))
	                {
	                    x.innerHTML = "Not a valid email";
	                    x.style.color = "red"
	                }
	            else
	                {
	                    x.innerHTML = "Looks good!";
	                    x.style.color = "green"
	                }
	            }
</script>
<script type="text/javascript">
	$(document).ready(function(){
	            $('#add_charges').on('click', function(){
	      
	                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file" name="" id="banner123" class="form-control"></div>'+
	                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
	                $('#charges').append(html);
	            });
	      
	      $('.optionBox').on('click','.remove',function() {
	          $(this).parent().remove();
	          });
	        });
</script>
<script type="text/javascript">
	$(document).ready(function(){
	            $('#add_expert').on('click', function(){
	      
	                var html = '<div class="row addrow"><div class="col-sm-10"><input type="text" name="" id="expert_part" class="form-control"></div>'+
	                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
	                $('#expert').append(html);
	            });
	      $('.expert_new').on('click','.remove',function() {
	          $(this).parent().remove();
	          });
	        });
</script>
<!-----------Select Variant-->
<script type="text/javascript">
	$(document).ready(function(){
	            $('#add_more').on('click', function(){
	      
	                var html = '<div class="row addrow"><div class="form-group"><label for="" class="col-sm-2 control-label">Enter Statement</label><div class="col-sm-10"><input type="text" name="" id=""  class="form-control"></div></div>'+ 
	          '<div class="form-group"><label for="" class="col-sm-2 control-label">Enter Label</label><div class="col-sm-10"><input type="text" name="" id="" class="form-control"></div></div>'+
	                '<div class="form-group"><label for="" class="col-sm-2 control-label">Count</label><div class="col-sm-10"><button class="btn btn-default">1</button><button class="btn btn-default">2</button><button class="btn btn-default">3</button><button class="btn btn-default">4</button><button class="btn btn-default">5</button><button class="btn btn-default">6</button><button class="btn btn-default">7</button><button class="btn btn-default">8</button><button class="btn btn-default">9</button><button class="btn btn-default">10</button></div>  </div>'+
	                '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
	                $('#variant').append(html);
	            });
	      $('.variant_new').on('click','.remove',function() {
	          $(this).parent().remove();
	          });
	        });
</script>
<!----------Add Service Details-------->
<script type="text/javascript">
	$(document).ready(function(){
	            $('#add_details_banner').on('click', function(){
	      
	                var html = '<div class="row addrow"><div class="col-sm-10"><input type="file" name="" id="banner123" class="form-control"></div>'+
	                    '<div class="col-sm-1"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button> </div>';
	                $('#service_part').append(html);
	            });
	      $('.details_service').on('click','.remove',function() {
	          $(this).parent().remove();
	          });
	        });
</script>
<script type="text/javascript">
	$(document).ready(function(){
	            $('#add_bullet').on('click', function(){
	      
	                var html = '<div class=""> <label for="" class="col-sm-2 control-label"></label><div class="col-sm-10"><label>Title</label><input type="text" name="" id="banner" class="form-control">'+
	                    '<label>Description</label><textarea class="form-control"></textarea><button type="button" class="btn btn-danger pull-right remove"><i class="fa fa-trash-o"></i></button> </div>';
	                $('#bullet_part').append(html);
	            });
	      $('.bullet_part').on('click','.remove',function() {
	          $(this).parent().remove();
	          });
	        });
</script>
<!--   <script type="text/javascript">
  function onButtonClick(){
 var element = document.getElementById("servicebox");
  element.classList.remove("boxhide");
 element.classList.add("boxshow");
}
    </script> -->
<script type="text/javascript">
	$("#openservice").click(function(){
	  $("#servicebox").show();
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
	            $('#add_variant').on('click', function(){
	      
	                var html = '<div class="row mt-10"><div class="col-sm-6"><input type="text" name="" id="" class="form-control"></div><div class="col-sm-4"><label class="checkbox-inline"><input type="checkbox" name="">Consultant Visit</label></div><div class="col-sm-2"></div><button type="button" class="btn btn-danger remove"><i class="fa fa-trash-o"></i></button></div>';
	                $('#add_variantbox').append(html);
	            });
	      $('.add_variantbox').on('click','.remove',function() {
	          $(this).parent().remove();
	          });
	        });
</script>
<script type="text/javascript">
	function yesnoCheck() {
	    if (document.getElementById('yesCheck').checked) {
	        document.getElementById('consultant_booking').style.display = 'block';
	    }
	    else document.getElementById('consultant_booking').style.display = 'none';
	
	}
</script>
<script type="text/javascript">
	function myFunction() {
	  // Get the checkbox
	  var checkBox = document.getElementById("myCheck");
	  // Get the output text
	  var consultant_booking = document.getElementById("consultant_booking");
	
	  // If the checkbox is checked, display the output text
	  if (checkBox.checked == true){
	    consultant_booking.style.display = "block";
	  } else {
	    consultant_booking.style.display = "none";
	  }
	}
</script>
<script type="text/javascript">
	function befsertime() {
	    if (document.getElementById('befsertime').checked) {
	        document.getElementById('ser_reqbox').style.display = 'block';
	    }
	    else document.getElementById('ser_reqbox').style.display = 'none';
	
	}
</script>
<!-- add reshedule booking -->
<script type="text/javascript">
	$(document).ready(function(){
	            $('#add_resbook').on('click', function(){
	
	                var html = '<div class="row mt-10"><div class="col-md-5"><label for="" class="control-label"> Description</label><input type="text" class="form-control" id="" name=""></div><div class="col-md-3"><label for="" class="control-label"> Hours</label><input type="text" class="form-control" id=""  name=""></div><div class="col-md-3"><label for="" class="control-label"> Amount</label><input type="text" class="form-control" id=""  name=""></div><button type="button" class="btn btn-danger remove" style="margin-top:26px;"><i class="fa fa-trash-o"></i></button></div>';
	                $('#reschedule_box').append(html);
	            });
	      $('.reschedule_box').on('click','.remove',function() {
	          $(this).parent().remove();
	          });
	        });
</script>
<!-- add cancellation booking -->
<script type="text/javascript">
	$(document).ready(function(){
	            $('#add_canbook').on('click', function(){
	
	                var html = '<div class="row mt-10"><div class="col-md-5"><label for="" class="control-label"> Description</label><input type="text" class="form-control" id="" name=""></div><div class="col-md-3"><label for="" class="control-label"> Hours</label><input type="text" class="form-control" id=""  name=""></div><div class="col-md-3"><label for="" class="control-label"> Amount</label><input type="text" class="form-control" id=""  name=""></div><button type="button" class="btn btn-danger remove" style="margin-top:26px;"><i class="fa fa-trash-o"></i></button></div>';
	                $('#canbook_box').append(html);
	            });
	      $('.canbook_box').on('click','.remove',function() {
	          $(this).parent().remove();
	          });
	        });
</script>
<!-- penalty -->
<script type="text/javascript">
	$(document).ready(function(){
	            $('#add_penalty').on('click', function(){
	
	                var html = '<div class="row mt-10"><div class="col-md-5"><label for="" class="control-label"> Description</label><input type="text" class="form-control" id="" name=""></div><div class="col-md-3"><label for="" class="control-label"> Hours</label><input type="text" class="form-control" id=""  name=""></div><div class="col-md-3"><label for="" class="control-label"> Amount</label><input type="text" class="form-control" id=""  name=""></div><button type="button" class="btn btn-danger remove" style="margin-top:26px;"><i class="fa fa-trash-o"></i></button></div>';
	                $('#penalty_box').append(html);
	            });
	      $('.penalty_box').on('click','.remove',function() {
	          $(this).parent().remove();
	          });
	        });
</script>
