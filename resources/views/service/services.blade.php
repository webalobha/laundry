@extends('layout.app')
@section('title') Manage Services  @stop
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <!-- <h1>
         Services
         </h1> -->
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Services</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <!-- /.row -->
     
      <div class="row">
         <div class="col-md-12">
            <ul class="nav nav-tabs">
               <li class="nav-item active">
                  <a class="nav-link active" data-toggle="tab" href="#home" style="width: 200px; margin-right: 20px;">
                     <div class="icon_tab">
                        <i class="fa fa-tint"></i>
                        <h4>Wash And Fold</h4>
                     </div>
                  </a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#menu1" style="width: 200px; margin-right: 20px;">
                     <div class="icon_tab">
                        <i class="fa fa-tint"></i>
                        <h4>Dry Clean</h4>
                     </div>
                  </a>
               </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
               <div id="home" class="tab-pane active">
                  <br>
                  <div>
                     <ul class="nav nav-tabs">
                        <li class="nav-item active">
                           <a class="nav-link active" data-toggle="tab" href="#cloth">Clothes</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" data-toggle="tab" href="#bedding">Beddings</a>
                        </li>
                     </ul>
                     <!-- Tab panes -->
                     <div class="tab-content">
                        <div id="cloth" class="tab-pane active">
                           <br>
                           <div>
                              <form action="{{route('filter-service')}}" method="post" class="form-horizontal">
                              @csrf   
                                 <div class="row">
                                    <div class="col-md-3">
                                       <label>Search by item name</label>
                                       <input type="text" name="name" class="form-control">
                                       <input type="hidden" name="service_type_name" class="form-control" value="handdry">
                                    </div>
                                    <div class="col-md-3">
                                       <label>Status</label>
                                       <select class="form-control" name="status">
                                          <option value="">Select Status</option>
                                          <option value="1">On</option>
                                          <option value="0">Off</option>
                                       </select>
                                    </div>
                                    <div class="col-md-3">
                                       <div style="margin-top: 24px;">
                                          <a href="{{route('viewServices')}}" class="btn btn-default">Clear</a>
                                          <button type="submit" class="btn btn-main">Search</button>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                              <div class="row">
                                 <!-- /.box-header -->
                                 <div class="box-body">
                                    <div class="table-responsive">
                                       <table id="example1" class="table table-bordered table-striped">
                                          <thead>
                                             <tr>
                                                <th>S.No</th>
                                                <th>Item</th>
                                                <th>Image</th>
                                                <th>Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             @foreach($handDryservice as $key=>$handDryservice)
                                             <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$handDryservice->name}}
                                                </td>
                                                <td>
                                                   <img src="{{asset($handDryservice->item_image)}}" style="width: 100px; height: 100px;">
                                                </td>
                                                <td>
                                                   <button type="button" class="btn btn-primary btn-xs">Edit</button>
                                                   <label class="switch">
                                                      <input type="checkbox" onclick='serviceStatusChange("{{$handDryservice->id}}")' @if($handDryservice->status == '1') checked @endif>
                                                   <span class="slider round"></span>
                                                   </label>
                                                </td>
                                             </tr>
                                             @endforeach
                                             
                                             <tr>
                                             <form method="post" action="{{ url('/admin/postHangDry') }}" enctype="multipart/form-data">
                                               @csrf 
                                                    <td>{{ $handdry_count }}</td>
                                                   <td>
                                                      <input type="text" name="name" class="form-control" placeholder="Item Name" required="">
                                                   </td>
                                                   <td>
                                                      <span class="btn btn-main upload-btn" onclick="document.getElementById('getFile1').click()">Upload File</span>
                                                      <input type="file" name="item_image" class="form-control" id="getFile1" placeholder="Description" accept="image/jpg, image/jpeg, image/png" style="display: none;" required="">
                                                   </td>
                                                   <td>
                                                      <button type="submit" class="btn btn-main">Add</button>
                                                       <a href="{{route('viewServices')}}" class="btn btn-default">Clear</a>
                                                   </td>
                                             </form>
                                             </tr>
                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- /.box-body -->
                              </div>
                           </div>
                        </div>
                        <div id="bedding" class="tab-pane fade">
                           <br>
                           <div>
                               <form action="{{route('filter-service')}}" method="post" class="form-horizontal">
                               @csrf
                                 <div class="row">
                                    <div class="col-md-3">
                                       <label>Search by item name</label>
                                       <input type="text" name="name" class="form-control">
                                       <input type="hidden" name="service_type_name" class="form-control" value="bindding">
                                    </div>
                                    <div class="col-md-3">
                                       <label>Search by variant</label>
                                       <select class="form-control" name="variant">
                                          <option value="">-Select Variant-</option>
                                          <option value="Twin">Twin</option>
                                          <option value="Full">Full</option>
                                          <option value="King">King</option>
                                          <option value="Queen">Queen</option>
                                       </select>
                                    </div>
                                    <div class="col-md-3">
                                       <label>Status</label>
                                       <select class="form-control" name="status">
                                          <option value="">Select Status</option>
                                          <option value="1">On</option>
                                          <option value="0">Off</option>
                                       </select>
                                    </div>
                                    <div class="col-md-3">
                                       <div style="margin-top: 24px;">
                                          <a href="{{route('viewServices')}}" class="btn btn-default">Clear</a>
                                          <button type="submit" class="btn btn-main">Search</button>
                                       </div>
                                    </div>
                                 </div>
                              </form>
                              <div class="row">
                                 <!-- /.box-header -->
                                 <div class="box-body">
                                    <div class="table-responsive">
                                       <table id="example1" class="table table-bordered table-striped">
                                          <thead>
                                             <tr>
                                                <th>S.No</th>
                                                <th>Item</th>
                                                <th>Image</th>
                                                <th>Variant</th>
                                                <th>Action</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                              @foreach($binddingService as $key=>$binddingService)
                                             <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$binddingService->name}}
                                                </td>
                                                <td>
                                                   <img src="{{asset($binddingService->item_image)}}" style="width: 100px; height: 100px;">
                                                </td>
                                                <td>{{$binddingService->variantData}}</td>
                                                <td>
                                                   <button type="button" class="btn btn-primary btn-xs">Edit</button>
                                                   <label class="switch">
                                                      <input type="checkbox" onclick='serviceStatusChange("{{$binddingService->id}}")' @if($binddingService->status == '1') checked @endif>
                                                   <span class="slider round"></span>
                                                   </label>
                                                </td>
                                             </tr>
                                           @endforeach
                                             <tr>
                                             <form method="post" action="{{ url('/admin/postBedding') }}" enctype="multipart/form-data">
                                               @csrf 
                                                   <td>{{ $bindding_count }}</td>
                                                   <td>
                                                      <input type="text" name="name" class="form-control" placeholder="Item Name" required>
                                                   </td>
                                                   <td>
                                                      <span class="btn btn-main upload-btn" onclick="document.getElementById('getFile2').click()">Upload File</span>
                                                      <input type="file" name="item_image" class="form-control" id="getFile2" placeholder="Description" accept="image/jpg, image/jpeg, image/png" style="display: none;" required>
                                                   </td>
                                                   <td>
                                                      <select class="form-control" name="variant">
                                                         <option value="">-Select Variant-</option>
                                                         <option value="Twin">Twin</option>
                                                         <option value="Full">Full</option>
                                                         <option value="King">King</option>
                                                         <option value="Queen">Queen</option>
                                                      </select>
                                                   </td>
                                                   <td>
                                                      <button type="submit" class="btn btn-main">Add</button>
                                                       <a href="{{route('viewServices')}}" class="btn btn-default">Clear</a>
                                                   </td>
                                             </form>
                                             </tr>

                                          </tbody>
                                       </table>
                                    </div>
                                 </div>
                                 <!-- /.box-body -->
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div id="menu1" class="tab-pane fade">
                  <br>
                  <div>
                    <form action="{{route('filter-service')}}" method="post" class="form-horizontal">
                     @csrf
                        <div class="row">
                           <div class="col-md-3">
                              <label>Search by item name</label>
                              <input type="text" name="name" class="form-control">
                              <input type="hidden" name="service_type_name" value="dryclean" class="form-control">
                           </div>
                           <div class="col-md-3">
                              <label>Status</label>
                              <select class="form-control" name="status">
                                 <option value="">Select Status</option>
                                 <option value="1">On</option>
                                 <option value="0">Off</option>
                              </select>
                           </div>
                           <div class="col-md-3">
                              <div style="margin-top: 24px;">
                                 <a href="{{route('viewServices')}}" class="btn btn-default">Clear</a>
                                 <button type="submit" class="btn btn-main">Search</button>
                              </div>
                           </div>
                        </div>
                     </form>
                     <div class="row">
                        <!-- /.box-header -->
                        <div class="box-body">
                           <div class="table-responsive">
                              <table id="example1" class="table table-bordered table-striped">
                                 <thead>
                                    <tr>
                                       <th>S.No</th>
                                       <th>Item</th>
                                       <th>Image</th>
                                       <th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @foreach($DrycleanService as $key=>$DrycleanService)
                                    <tr>
                                       <td>{{$key+1}}</td>
                                       <td>{{$DrycleanService->name}}
                                       </td>
                                       <td>
                                          <img src="{{asset($DrycleanService->item_image)}}" style="width: 100px; height: 100px;">
                                       </td>
                                       <td>
                                          <button type="button" class="btn btn-primary btn-xs">Edit</button>
                                          <label class="switch">
                                             <input type="checkbox" onclick='serviceStatusChange("{{$DrycleanService->id}}")' @if($DrycleanService->status == '1') checked @endif>
                                          <span class="slider round"></span>
                                          </label>
                                       </td>
                                    </tr>
                                   @endforeach

                                  <tr>
                                    <form method="post" action="{{ url('/admin/postDryClean') }}" enctype="multipart/form-data">
                                     @csrf
                                          <td>{{ $dryclean_count }}</td>
                                          <td>
                                             <input type="text" name="name" class="form-control" placeholder="Item Name" required="">
                                          </td>
                                          <td>
                                             <span class="btn btn-main upload-btn" onclick="document.getElementById('getFile3').click()">Upload File</span>
                                             <input type="file" name="item_image" class="form-control" id="getFile3" placeholder="Description" accept="image/jpg, image/jpeg, image/png" style="display: none;" required="">
                                          </td>
                                          <td>
                                            <button type="submit" class="btn btn-main">Add</button>
                                              <a href="{{route('viewServices')}}" class="btn btn-default">Clear</a>
                                          </td>
                                    </form>
                                    </tr> 

                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <!-- /.box-body -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@stop

@section('javascript')
<script type="text/javascript">
function serviceStatusChange($id){
   var id=$id;
    $.ajax({
          headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
         url:'{{url("admin/serviceStatus")}}',
         method: 'POST',
         data: { id: id },
         dataType: 'json',
         success: function(response) {
           //$('#').val(response.responseData);
         }
 
     });
  }
</script>
@stop