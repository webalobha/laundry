<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

# User Registration Api
Route::prefix('user')->group(function() {
	Route::post('register', 'Api\User\RegisterController@register');
	Route::post('verifyOtp', 'Api\User\RegisterController@verifyOtp');
	Route::post('setPassword', 'Api\User\RegisterController@setPassword');
	Route::post('address/update', 'Api\User\RegisterController@updateAddress');
	Route::post('login', 'Api\User\LoginController@login');
});