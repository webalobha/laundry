<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


# Route Admin Panel login page.
Route::get('/admin', 'AdminController@index')->name('admin.index');

# Route Admin Login Page.
Route::post('/admin', 'AdminController@login');

// Admin Routes.
Route::group(['middleware' => 'admin', 'prefix' => 'admin'], function() {
	# Login Controller Routes.
	Route::get('login', 'AdminController@logout');

	# Dash Board Controller Routes.
	Route::get('dashboard', 'DashboardController@index')->name('home');

	# Service Controller Routes.
	Route::get('viewServices', 'ServiceController@viewServices')->name('viewServices');
	Route::any('/filter-service', 'ServiceController@viewServices')->name('filter-service');
	Route::post('postHangDry', 'ServiceController@postHangDry')->name('postHangDry');
	Route::post('postBedding', 'ServiceController@postBedding')->name('postBedding');
	Route::post('postDryClean', 'ServiceController@postDryClean')->name('postDryClean');
	Route::post('serviceStatus','ServiceController@serviceStatus')->name('serviceStatus');


}); // End Admin Routes.