<?php

namespace App\Http\Controllers;

use Auth;
use App\Model\Admin;
use Illuminate\Http\Request;
use App\Http\Traits\MessageStatusTrait;

class AdminController extends Controller
{
    # use message status trait.
    use MessageStatusTrait;

    #Bind the Admin Model.
    protected $admin;

    # Bind view path.
    protected $view = 'layout.';

    # Bind the message variable name.
    protected $type  =  'Admin ';

    /**
     *
     * Define the constructor of controller.
     * @param Admin $admin
     */
    public function __construct(Admin $admin)
    {
        $this->admin   =  $admin;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::guard('admin')->check() ) {
           return redirect()->route('home');
           //return redirect('/superadmin/dashboard');
        }else{
        # redirect to login page.
        return view($this->view.'login');
       } 
    }

    /**
     * Do login user with his username name and password.
     * 
     * @param Request $request.
     */
    public function login(Request $request)
    {
        # check validation of credentials.
        $validatedData  =   $request->validate([
                                        'email'     => 'required',
                                        'password'      => 'required',
                                    ]);

        # get user name password credentials
        $credentials = $request->only('email', 'password');

        if (!Auth::guard('admin')->attempt($credentials)) {
            # redirect back to login page with session message.
           return  redirect()->back()
                            ->withErrors([$this->credentialNotMatchKey => $this->credentialNotMatch()])->withInput();
        } else {
            # get Login User
            $user = Auth::guard('admin')->user();

            # Check login user is active or not.
            if($user != '') {
                # redirect to home page with success message inside session.
                return  redirect()->route('home')
                                    ->withErrors([$this->signInSuccessfullyKey => $this->signInMessaage()]);
            } else {
                
                Auth::guard('admin')->logout();
                # redirect back to login page with session message.
                return  redirect()->back()
                                ->withErrors([$this->userNotActiveKey => $this->userNotAciveMessage()]);
            } // end else of status.
        } // End Else of credentials.
    }
    
    /**
     *
     * Logout login admin user.
     * @param 
     */
    public function logout()
    { 
        # Authenticate user logout! Session flush.
        Auth::guard('admin')->logout();

        # redirect to login page.
        return $this->index();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
