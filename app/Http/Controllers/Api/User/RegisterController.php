<?php

namespace App\Http\Controllers\Api\User;

# Models
use App\Model\User; 
use App\Model\City; 
use App\Model\State; 
use App\Model\Country; 
use App\Model\UserOtp; 
use App\Model\UserAddress; 

# Third Party Package
use PragmaRX\ZipCode\ZipCode;

# Traits
use App\Http\Traits\StatusTrait;

# Vendor Classes
use DB;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    use StatusTrait;

    # Variable to Bind Model
    protected $user;

    # bind UserOtp Model
    protected $userOtp;

    # bind Country Model
    protected $country;

    # bind State Model
    protected $state;

    # bind City Model
    protected $city; 

    # bind UserAddress Model
    protected $userAddress;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        User $user,
        UserOtp $userOtp, Country $country, State $state, City $city,
        UserAddress $userAddress)
    {
       
        $this->user                     = $user;
        $this->city                     = $city;
        $this->state                    = $state;
        $this->userOtp                  = $userOtp;
        $this->country                  = $country;
        $this->userAddress              = $userAddress;
    }

    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'mobile_number'     => 'required|numeric',
            'email'             => 'required|string',
            'name'              => 'required|string',
            'device_type'       => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        DB::beginTransaction();

        # Fetch all the inputs
        $input =  $request->all(); 

        # Check if email or number Already Exist
        $mobileNumber   = $input['mobile_number'];
        $email          = $input['email'];

        # check user already Exist on that Email
        $users = $this->user
                      ->where('mobile_number', $mobileNumber)
                      ->orWhere('email', $email)
                      ->get();
        
        # return response if User already exist on requested Email
        if($users->isNotEmpty()) {
            $otp = mt_rand(1000,9999);

            # fetch the First User
            $user = $users->first();

            # Update User Device Type
            $user->update(['device_type' => $request->get('device_type')]);

            # Set the success message after User creation 
            $data = [
                'user_id'           =>  (string)$user->id,
                'mobile_number'     =>  (string)$user->mobile_number,
                'email'             =>  (string)$user->email,
                'otp'               =>  (string)$otp
            ];

            $otpData   = ['user_id' => $user->id, 'otp' => $otp];

            # Create User Otp Model
            $this->userOtp->create($otpData);

            DB::commit();

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'User already exist!!!',
                'data'      =>  $data
             ]); 
        } else {
            # Create User Model
            $user =  $this->user->create([
                'mobile_number' => $mobileNumber,
                'email'         => $email,
                'first_name'    => $request->name,
                'device_type'   => $request->get('device_type')
            ]);

            #Set data for User Otp
            $otp    = mt_rand(1000,9999);
            $data   = ['user_id' => $user->id, 'otp' => $otp];

            # Create User Otp Model
            $this->userOtp->create($data);

            # Set the success message after User creation 
            $data = [
                'mobile_number'     =>  $user->mobile_number,
                'email'             =>  $user->email,
                'userId'            =>  (string)$user->id,
                'otp'               =>  (string)$otp
            ];

            DB::commit();

            # return response
            return response()->json([
                'code'      => (string)$this->successStatus, 
                'message'   => 'User has been register Successfully!!!',
                'data'      => $data
             ]); 

        }
    }

    /**
     * @method to verify Users Request Otp
     * 
     * @return Otp Verified or Not
     */
    public function verifyOtp(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'     => 'required|numeric',
            'otp'         => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Fetch all the inputs
        $input =  $request->all(); 

        # Check if email Already Exist
        #$mobileNumber = $input['mobile_number'];

        # check user already Exist on that id
        $user = $this->user
                      ->with('otp')
                      ->where('id', $request->get('user_id'))
                      ->get()
                      ->last();

        # return response if User already exist on requested Email
        if($user != '') {
            # fetch the Otp of Users
            $userOtps = $user->otp;
            if($userOtps->isNotEmpty() ) {
                $lastOtp = $userOtps->last()->otp;
                if($lastOtp == $request->get('otp')) {

                    # Update uSer token
                    $user->update(['api_token' => $token]);

                    # Set the Data
                    $data = [
                        'mobile_number'     => $user->mobile_number,
                        'user_id'           => $user->id,
                        'name'              => $user->first_name??'',
                        'email'             => $user->email??'',
                        'latitude'          => $user->latitude??'',
                        'longitude'         => $user->longitude??'',  
                       
                    ];

                    # return response
                    return response()->json([
                        'code'      => (string)$this->successStatus, 
                        'message'   => 'Otp has been Verified.',
                        'data'      => $data
                     ]);
                } else {
                    # return response
                    return response()->json([
                        'code'      => (string)$this->failedStatus, 
                        'message'   => 'Otp does not match.',
                        'data'      => []
                     ]);
                }
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Otp Not Found.',
                    'data'      => []
                 ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found on Provided Id.',
                'data'      => []
             ]); 
        }
    }
    
    /**
     * function to generate the Token
     * 
     * @return Token
     */
    public function generateToken()
    {
        # Set the token 
        $token = Str::random(60);

        # Hash Token
        $hashToken = hash('sha256', $token);

        # return the Hash Token 
        return $hashToken;
    }

    /**
     * function to Set Password
     * 
     * @return Token
     */
    public function setPassword(Request $request)
    {
       # Validate request data
        $validator = Validator::make($request->all(), [ 
            'user_id'     => 'required|numeric',
            'password'         => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # check user already Exist on that id
        $user = $this->user
                      ->with('otp')
                      ->where('id', $request->get('user_id'))
                      ->get()
                      ->last();

        # Check User presence
        if($user != '') {
            $password = $request->get('password');
            if(strlen($password) < 8 OR strlen($password) > 20) {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Password cannot less than 8 and not more than 20 characters.',
                    'userId'    =>  $user->id,
                    'data'      => []
                ]); 
            } else {
                $password = Hash::make($password);
                $user->update(['password' => $password]);
                # return response
                return response()->json([
                    'code'      => (string)$this->successStatus, 
                    'message'   => 'User Password as been updated Successfully.',
                    'userId'    =>  $user->id,
                    'data'      => []
                ]);
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found on Provided Id.',
                'data'      => []
            ]); 
        }
    }

    /**
     * function to Update Address of User
     * 
     * @return Token
     */
    public function updateAddress(Request $request)
    {
       # Validate request data
        $validator = Validator::make($request->all(), [ 
            'userId'           => 'required|numeric',
            'addressType'       => 'required|numeric',
            'houseNo'           => 'required|numeric',
            'streetAddress'     => 'required|string',
            'city'              => 'required|string',
            'state'             => 'required|string',
            'zipCode'           => 'required|numeric',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # check user already Exist on that id
        $user = $this->user
                      ->where('id', $request->get('userId'))
                      ->get()
                      ->last();

        # Check User presence
        if($user != '') {
           # fetch State
            $state = ucwords($request->get('state'));

            # Fetch the State from Database
            $fetchState = $this->state
                               ->with('cities')
                               ->where('name', $state)
                               ->get();

            if($fetchState->isNotEmpty()) {
                # fetch City
                $city = ucwords($request->get('city'));
                $fetchCity = $fetchState->first()
                                         ->cities
                                         ->where('name', $city);
                if($fetchCity->isNotEmpty()) {
                    $data = [
                        'user_id'       => $user->id ?? '' ,
                        'state_id'      => $fetchState->first()->id ?? '' ,
                        'city_id'       => $fetchCity->first()->id ?? '' ,
                        'zip_code'      => $request->get('zipCode') ?? '' ,
                        'house_no'      => $request->get('houseNo') ?? '' ,
                        'street'        => $request->get('streetAddress') ?? '' ,
                        'unit_no'       => $request->get('unitNo') ?? '' ,
                        'address_type'  => $request->get('addressType') ?? '' ,
                        'status'        => true ,
                        'longitude'     => $request->get('longitude') ?? '' ,
                        'latitude'      => $request->get('latitude') ?? '' ,
                    ];

                    # Create Address for User
                    $this->userAddress->create($data);

                    # return response
                    return response()->json([
                        'code'      => (string)$this->successStatus, 
                        'message'   => 'Address Save Successfully.',
                        'data'      => []
                    ]);
                } else {
                    # return response
                    return response()->json([
                        'code'      => (string)$this->failedStatus, 
                        'message'   => 'City Not Found in State, Please correct Spelling.',
                        'data'      => []
                    ]);
                }
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'State Not Found, Please correct Spelling.',
                    'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found on Provided Id.',
                'data'      => []
            ]); 
        }
    }
}
