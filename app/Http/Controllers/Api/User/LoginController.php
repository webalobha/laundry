<?php

namespace App\Http\Controllers\Api\User;

# Models
use App\Model\User; 
use App\Model\City; 
use App\Model\State; 
use App\Model\Country; 
use App\Model\UserOtp; 
use App\Model\UserAddress; 

# Traits
use App\Http\Traits\StatusTrait;

# Vendor Classes
use DB;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    use StatusTrait;

    # Variable to Bind Model
    protected $user;

    # bind UserOtp Model
    protected $userOtp;

    # bind Country Model
    protected $country;

    # bind State Model
    protected $state;

    # bind City Model
    protected $city; 

    # bind UserAddress Model
    protected $userAddress;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        User $user,
        UserOtp $userOtp, Country $country, State $state, City $city,
        UserAddress $userAddress)
    {
       
        $this->user                     = $user;
        $this->city                     = $city;
        $this->state                    = $state;
        $this->userOtp                  = $userOtp;
        $this->country                  = $country;
        $this->userAddress              = $userAddress;
    }

    /**
     * @method to login the User
     * @param Request $request
     * @return json
     */
    public function login(Request $request)
    {
        # Validate request data
        $validator = Validator::make($request->all(), [ 
            'mobileOrEmail'     => 'required|string',
            'password'          => 'required|string',
        ]);

        # If validator fails return response
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        # Fetch the User
        $mobileOrEmail = $request->get('mobileOrEmail');

        # fetch User from Database
        $user = $this->user
                     ->where('mobile_number', $mobileOrEmail)
                     ->orWhere('email', $mobileOrEmail)
                     ->get();
        
        if($user->isNotEmpty()) {
            $user = $user->last();
            if(Hash::check($request->get('password'), $user->password)) {
                $otp = mt_rand(1000,9999);
                $data = [
                    'user_id'   => $user->id,
                    'otp'       => $otp,
                ];

                # Create User Otp
                $this->userOtp->create($data);
            } else {
                # return response
                return response()->json([
                    'code'      => (string)$this->failedStatus, 
                    'message'   => 'Entered Password is wrong please try Again',
                    'data'      => []
                ]); 
            }
        } else {
            # return response
            return response()->json([
                'code'      => (string)$this->failedStatus, 
                'message'   => 'User not Found on Provided Id.',
                'data'      => []
            ]); 
        }

    }
    

    /**
     * function to generate the Token
     * 
     * @return Token
     */
    public function generateToken()
    {
        # Set the token 
        $token = Str::random(60);

        # Hash Token
        $hashToken = hash('sha256', $token);

        # return the Hash Token 
        return $hashToken;
    }
}
