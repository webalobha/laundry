<?php

namespace App\Http\Controllers;
use Response;
use Validator;
use File;
use App\Model\Service;
use App\Model\ServiceType;
use App\Model\ServiceItem;
use App\Model\ServiceItemVariant;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    # Bind view path.
    protected $view = 'service.';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewServices(Request $request)
    {
       
        $handDryservice=ServiceItem::where('service_id', '1')
                        ->where('service_type_id', '1')
                        ->orderBy('id','DESC');
         if($request->service_type_name == 'handdry'){
           if(isset($request->name) && $request->name != ''){
            $handDryservice->where('name','LIKE','%'.$request->name.'%');
           }
           if(isset($request->status) && $request->status != ''){
            $handDryservice->where('status',$request->status);
           }
         }     
        $handDryservice = $handDryservice->get(); 
        $handdry_count = count($handDryservice)+1;


        
        $binddingService=ServiceItem::where('service_id', '1')
                        ->where('service_type_id', '2')
                        ->orderBy('id','DESC');
                        //->get();
          if($request->service_type_name == 'bindding'){
           if(isset($request->name) && $request->name != ''){
            $binddingService->where('name','LIKE','%'.$request->name.'%');
           }
           if(isset($request->status) && $request->status != ''){
            $binddingService->where('status',$request->status);
           }

           if(isset($request->variant) && $request->variant != ''){

            $variantService=ServiceItemVariant::where('name', $request->variant)->get();
            $serviceIdData[]="";
            foreach ($variantService as $variantName) {
                $serviceIdData[]=$variantName->service_item_id;
            }

            $binddingService->whereIn('id',$serviceIdData);

           }

         }   
        
        $binddingService = $binddingService->get();                 
        $bindding_count = count($binddingService)+1;   

        

        $DrycleanService=ServiceItem::where('service_id', '2')
                        ->orderBy('id','DESC');
         if($request->service_type_name == 'dryclean'){
           if(isset($request->name) && $request->name != ''){
            $DrycleanService->where('name','LIKE','%'.$request->name.'%');
           }
           if(isset($request->status) && $request->status != ''){
            $DrycleanService->where('status',$request->status);
           }
         } 
        $DrycleanService = $DrycleanService->get();  
        $dryclean_count = count($DrycleanService)+1;                                              
        # redirect to home page.
        return view($this->view.'services')->with(['handDryservice'=>$handDryservice,'binddingService'=>$binddingService,'DrycleanService'=>$DrycleanService,'handdry_count'=>$handdry_count,'bindding_count'=>$bindding_count,'dryclean_count'=>$dryclean_count]);
    }

  
    /**
     * Store Hand Dry Service Items.
     */
    public function postHangDry(Request $request)
    {
       $data = ['name' => 'required'];

        # validation check
        $validator = \Validator::make($request->all() , $data);

        if ($validator->fails())
        {
            return redirect()->back()->with('error', 'Required Fields are missing.');
        }
        else
        {

            $itemCheck = ServiceItem::where('name', $request->name)
                ->where('service_id', '1')
                ->where('service_type_id', '1')
                ->first();
            if ($itemCheck)
            {
                return redirect()->back()->with('error', 'Sorry,This Service Item Already Exist.');
            }
            try
            {
                $addItem = new ServiceItem();
                $addItem->name = $request->name;
                $addItem->service_id = '1';
                $addItem->service_type_id = '1';
                $addItem->status = '1';

                #image
                if($request->hasfile('item_image'))
                {
                    $file = $request->file('item_image');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $filename =((string)(microtime(true)*10000)).'.'.$extension;
                    $file->move(public_path('assets/image/service_image/'), $filename);
                    $addItem->item_image='assets/image/service_image/'.$filename;
                }else{
                    return redirect()->back()->with('error', 'Image is Required.');
                }

                #data save in db
                $addItem->save();
                return redirect()->back()->with('success', 'Hang Dry Service Item Added Successfully.');
            }
            catch(\Exception $e)
            {
                //dd($e);
                return redirect()->back()->with('error', 'Something Went Wrong.');
            }
        }
    }


   /**
     * Store Bedding Service Items.
     */
    public function postBedding(Request $request)
    {
       $data = ['name' => 'required'];

        # validation check
        $validator = \Validator::make($request->all() , $data);

        if ($validator->fails())
        {
            return redirect()->back()->with('error', 'Required Fields are missing.');
        }
        else
        {

            $itemCheck = ServiceItem::where('name', $request->name)
                ->where('service_id', '1')
                ->where('service_type_id', '2')
                ->first();
            if ($itemCheck)
            {
                return redirect()->back()->with('error', 'Sorry,This Service Item Already Exist.');
            }
            try
            {
                $addItem = new ServiceItem();
                $addItem->name = $request->name;
                $addItem->service_id = '1';
                $addItem->service_type_id = '2';
                $addItem->status = '1';
                if(!empty($request->variant)){
                 $addItem->is_variant_avail = '1';
                }

                #image
                if($request->hasfile('item_image'))
                {
                    $file = $request->file('item_image');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $filename =((string)(microtime(true)*10000)).'.'.$extension;
                    $file->move(public_path('assets/image/service_image/'), $filename);
                   $addItem->item_image='assets/image/service_image/'.$filename;
                }else{
                    return redirect()->back()->with('error', 'Image is Required.');
                }
                #data save in db
                $addItem->save();

               // varient check and add variant table
                if(!empty($request->variant)){
                    $addvariant = new ServiceItemVariant();
                    $addvariant->name = $request->variant;
                    $addvariant->service_item_id =  $addItem->id;
                    $addvariant->save();
                }
                return redirect()->back()->with('success', 'Bedding Service Item Added Successfully.');
            }
            catch(\Exception $e)
            {
                dd($e);
                return redirect()->back()->with('error', 'Something Went Wrong.');
            }
        }
    }


   /**
     * Store Dry Clean Service Items.
     */
    public function postDryClean(Request $request)
    {
       $data = ['name' => 'required'];

        # validation check
        $validator = \Validator::make($request->all() , $data);

        if ($validator->fails())
        {
            return redirect()->back()->with('error', 'Required Fields are missing.');
        }
        else
        {

            $itemCheck = ServiceItem::where('name', $request->name)
                ->where('service_id', '2')
                ->first();
            if ($itemCheck)
            {
                return redirect()->back()->with('error', 'Sorry,This Service Item Already Exist.');
            }
            try
            {
                $addItem = new ServiceItem();
                $addItem->name = $request->name;
                $addItem->service_id = '2';
                $addItem->status = '1';

                #image
                if($request->hasfile('item_image'))
                {
                    $file = $request->file('item_image');
                    $extension = $file->getClientOriginalExtension(); // getting image extension
                    $filename =((string)(microtime(true)*10000)).'.'.$extension;
                    $file->move(public_path('assets/image/service_image/'), $filename);
                   $addItem->item_image='assets/image/service_image/'.$filename;
                }else{
                    return redirect()->back()->with('error', 'Image is Required.');
                }

                #data save in db
                $addItem->save();
                return redirect()->back()->with('success', 'Dry Clean Service Item Added Successfully.');
            }
            catch(\Exception $e)
            {
                //dd($e);
                return redirect()->back()->with('error', 'Something Went Wrong.');
            }
        }
    }


    /**
     * active deactive
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function serviceStatus(Request $request)
    {
        # get the status
        $status = ServiceItem::where('id', $request->id)->first()->status;
        # check status, if active
        if($status == '1')
        {
            $statusCode = '0'; # deactive( update status to zero)
        }else{
            $statusCode = '1'; # active( update status to one)
        }

        # update status code
        ServiceItem::where('id', $request->id)->update(['status' => $statusCode]);
       
       return Response::json(array('responseCode' => '200'));
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
