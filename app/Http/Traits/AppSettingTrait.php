<?php

namespace App\Http\Traits;

use App\Models\AppSetting;

trait AppSettingTrait 
{

	/**
	 * display the app setting date time formate.
	 * 
	 * @param $date.
	 */
    public function dateTimeFormat($date)
    {
        # get App settings.
        $appSetting = AppSetting::with('dateTimeFormat')->first();

        # get date time string format.
        $dateTimeFormatString   =   ($appSetting->dateTimeFormat)  ? $appSetting->dateTimeFormat->name  : 'yyyy/dd/mm';

        # convert date into according date settings.
        $convertedDate  =  date($dateTimeFormatString, strtotime($date));

        # return new date.
        return $convertedDate;
    } 
	
}