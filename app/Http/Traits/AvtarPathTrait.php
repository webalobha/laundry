<?php

namespace App\Http\Traits;

trait AvtarPathTrait 
{
	# Bind the destination path.
	protected $destinationPath = '/public/assets/image/';

	# Bind the avtar path.
	protected $avtarPath = '/assets/image/';

	/**
	 * Define the user avtar destination path.
	 * 
	 * @param 
	 */
	public function storeDestinationPath()
	{
		# return the user detination path.
		return $this->destinationPath.'stores';
	}

	/**
	 * Define the user avtar destination path.
	 * 
	 * @param 
	 */
	public function storeAvtarPath()
	{
		# return the avtar path.
		return $this->avtarPath.'stores/';
	}

	/**
	 * Define the user user destination path.
	 * 
	 * @param 
	 */
	public function userDestinationPath()
	{
		# return the user detination path.
		return $this->destinationPath.'users';
	}

	/**
	 * Define the user avtar destination path.
	 * 
	 * @param 
	 */
	public function userAvtarPath()
	{
		# return the avtar path.
		return $this->avtarPath.'users/';
	}

	/**
	 * Define the user avtar destination path.
	 * 
	 * @param 
	 */
	public function productDestinationPath()
	{
		# return the user detination path.
		return $this->destinationPath.'product_images';
	}

	/**
	 * Define the user avtar destination path.
	 * 
	 * @param 
	 */
	public function productAvtarPath()
	{
		# return the avtar path.
		return $this->avtarPath.'product_images/';
	}
	
}