<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceItem extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
	protected $table='service_items';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_id', 
        'service_type_id', 
        'name',
        'item_image', 
		'status' ,   
        'is_variant_avail' ,   
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * The attributes that should be mutated to casts.
     *
     * @var array
     */
    protected $casts = [
        'status',
        'is_variant_avail',
    ];



   /**
     * relation with Variants
     * @param
     * @return
     */
    public function variantName()
    {
      return $this->belongsTo(\App\Model\ServiceItemVariant::class, 'id', 'service_item_id');
    }

   /**
     * Attribuite for the relation Plan
     * @param
     * @return
     */
    public function getvariantDataAttribute()
    {
      if ($this->variantName != '') {
        return $this->variantName->name;
      } else {
        return 'NA';
      }
    }



}
