<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceItemVariant extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
	protected $table='service_item_variants';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_item_id', 
        'name', 
		'status' ,   
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * The attributes that should be mutated to casts.
     *
     * @var array
     */
    protected $casts = [
        'status',
    ];
}
