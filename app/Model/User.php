<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{

    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;

     /**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table='users';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name', 
        'mobile_number',
        'email',
        'status',
        'age',
        'gender',
        'latitude',
        'longitude',
        'device_type',
        'android_key',
        'ios_key',
        'password',
        'api_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

     /**
     * Model has Many Otp
     * 
     * @return relation
     */
    public function otp()
    {
        return $this->hasMany('App\Model\UserOtp', 'user_id', 'id');
    }
}
