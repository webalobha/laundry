<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends Model
{
    /**
    * The database table used by the model.
    *
    * @var string
    */
	protected $table='services';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
		'icon_name',       
		'icon_path',      
		'service_type_available',
		'status',   
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];

    /**
     * The attributes that should be mutated to casts.
     *
     * @var array
     */
    protected $casts = [
        'status',
        'service_type_available',
    ];
}
