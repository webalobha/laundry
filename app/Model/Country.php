<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
 // use SoftDeletes;
  
  # define table
  protected $table ='countries';
  
  # define fillable fields
  protected $fillable = [
  	                   'sort_name',
  	                   'name',
  	                   'phone_code',
  ];

  /**
   * Model have many States
   * @param
   * @return relation
   */
  public function states()
  {
  	return $this->hasMany('App\Model\State');
  }
}
