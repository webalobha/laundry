<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SupportTicketStatus extends Model
{
  
  # define table
  protected $table ='support_ticket_status';
  
  # define fillable fields
  protected $fillable = [
  	                   'name',
  ];
}
