<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Admin extends Authenticatable
{

    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;

     /**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table='admin';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
		'phone_number',       
		'country' ,      
		'state',
		'email' ,   
		'avtar' ,      
		'avtar_path'  ,   
		'company_name'  ,   
		'gst_number'  ,   
		'longitude'  ,   
		'latitude'  ,   
		'address'  ,   
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
    ];
}
